Game JAM CSUI

Game Title: Badass Santa
Genre: Action
Target Audience: 13+ Years Old
Platform: Windows PC , Linux

Klauce is a member of an organization called the Federation of Multi Dimensional Santa who works in the Tybald division. Tybald is a medieval dimension that is being dominated by monsters. One day, when Klauce is on a hurry,  his  sledge was damaged and fell near the monster's lair. The monsters took gifts on Klauce's sled. Klauce was furious and immediately entered the monster's lair to take his gifts back and kill all the monsters.

Rules:

as Klauce, player must eliminate all monsters and collect 10 gifts across levels with given time to win. If player is dead or the time is up, game is over and you lose

Features:

- Player will have 50 lives through the game and can be replenish by picking up health potion

- 3 different enemies with different attack damage and lives

Diversities:

All we had to do was follow the damn train	I Can’t Punch You Without Getting Closer	Night of Fire


Assets:
1. (Dungeon Tileset 32x32) https://stealthix.itch.io/dungeon-tileset-32x32-pxt
2. (Santa Sprite by elthen) https://elthen.itch.io/2d-pixel-art-santa-sprites3
3. (Jesse skeleton pack) https://jesse-m.itch.io/skeleton-pack
4. (dungeon collectibles) https://elthen.itch.io/2d-pixel-art-dungeon-collectables
5. (animated slime) https://rvros.itch.io/pixel-art-animated-slime
6. (sound effects):

- http://soundbible.com/995-Jab.html 

- http://soundbible.com/996-Kick.html 

- http://soundbible.com/828-Blood-Squirt.html

- http://soundbible.com/tags-whoosh.html 

-https://opengameart.org/content/slimy-monster-or-murder-sounds9 

- https://freesound.org/people/DrMinky/sounds/167074/ 

- https://opengameart.org/content/osare-minotaur-sounds

7. (minotaur sprite by elthen) https://elthen.itch.io/2d-pixel-art-minotaur-sprites

9. (BGMs)

- https://theartistunion.com/tracks/aced95
- https://www.youtube.com/watch?v=m5Y4RfumXho&t=45s