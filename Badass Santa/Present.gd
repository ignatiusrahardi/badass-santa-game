extends KinematicBody2D

var TYPE = "PRESENT"

func _on_pickupable_body_entered(body):
	if body.TYPE == "PLAYER":
		body.pickup = true
		body.gift += 1
		queue_free()

