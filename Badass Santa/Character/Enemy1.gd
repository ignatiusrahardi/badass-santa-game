extends "res://Character/Character.gd"


onready var animator = self.get_node("Sprite/AnimationPlayer")
onready var sprite = self.get_node("Sprite")
onready var player = self.get_parent().get_node("Player")
onready var atk = self.get_node("hit/CollisionShape2D")
onready var onrange = self.get_node("onrange/CollisionShape2D")

var randomove = 30
var reset = 0
var react_time = 300
var timer = 5
var target

var is_dead = false
var attacking = false
var count_dead = true


func _physics_process(delta):
	if HEALTH <= 0 and count_dead:
		dir.enemy_count -= 1
		count_dead = false
	elif HEALTH <= 0 and count_dead == false:
		dead()
	elif is_dead == false and HEALTH > 1:
		if attacking == false:
			movement()
			if self.modulate == Color(1,0,0):
				if timer > 0:
					timer -= 1
				else:
					self.modulate = Color(1,1,1)
			if target:
				animator.play("Walk")
				var chase = (player.global_position - global_position).normalized() * (SPEED+30)
				if player.global_position < global_position and atk.position.x > 0:
					sprite.flip_h = false
					atk.position.x = -atk.position.x - 4
					onrange.position.x = -onrange.position.x - 4
				elif player.global_position > global_position and atk.position.x < 0:
					sprite.flip_h = true
					atk.position.x = -atk.position.x - 4
					onrange.position.x = -onrange.position.x - 4
				move_and_slide(chase,  Vector2(0,0))
			else:
				patrol()
	


func dead():
	is_dead = true
	animator.play("Dead")
	yield(animator, "animation_finished")
	queue_free()
	set_physics_process(false)
	
	
func patrol():
	if reset > 0 :
		reset -= 1
	if reset == 0 or is_on_wall():
		direction = dir.rand()
		direction()
		reset = randomove
	
func _on_Vision_body_entered(body):
	if body == player:
		target = body
		
		
func _on_Vision_body_exited(body):
	if body == target:
		target = null
		
func direction() :
	if direction == Vector2.ZERO:
		animator.play("Idle")
	else:
		animator.play("Walk")
		if direction == Vector2.LEFT and atk.position.x > 0:
			sprite.flip_h = false
			atk.position.x = -atk.position.x - 4
			onrange.position.x = -onrange.position.x - 4
		elif direction == Vector2.RIGHT and atk.position.x < 0:
			sprite.flip_h = true
			atk.position.x = -atk.position.x - 4
			onrange.position.x = -onrange.position.x - 4

func take_damage(damage):
	HEALTH -= damage
	self.modulate = Color(1,0,0)

func attack(player):
	attacking = true
	animator.play("Attack")
	yield(animator , "animation_finished" )

func _on_hit_body_entered(body):
	if body == player:
		player.take_damage(DAMAGE)

func _on_hit_body_exited(body):
	pass

func _on_onrange_body_entered(body):
	if body == player:
		attack(body)


func _on_onrange_body_exited(body):
	if body == player:
		attacking = false
