extends "res://Character/Character.gd"

onready var sprite  = self.get_node("Idle")
onready var animator = self.get_node("anim")
onready var player = self.get_parent().get_node("Player")
onready var atk = self.get_node("hit/CollisionShape2D")
onready var onrange = self.get_node("onrange/CollisionShape2D")

var randomove = 30
var reset = 0
var react_time = 300
var timer = 5
var target
var is_dead = false
var attacking = false
var count_dead = true


func _physics_process(delta):
	if HEALTH <= 0 and count_dead:
		dir.enemy_count -= 1
		count_dead = false
	elif HEALTH <= 0 and count_dead == false:
		dead()
	elif is_dead == false:
		if attacking == false:
			movement()
			direction()
			if self.modulate == Color(1,0,0):
				if timer > 0:
					timer -= 1
				else:
					self.modulate = Color(1,1,1)
			if target:
				var chase = (player.global_position - global_position).normalized() * (SPEED+10)
				if player.global_position < global_position and atk.position.x > 0:
					sprite.flip_h = true
					atk.position.x = -atk.position.x - 4
					onrange.position.x = -onrange.position.x - 4
				elif player.global_position > global_position and atk.position.x < 0:
					sprite.flip_h = false
					atk.position.x = -atk.position.x - 4
					onrange.position.x = -onrange.position.x - 4
				move_and_slide(chase,  Vector2(0,0))
			else:
				patrol()

func dead():
	is_dead = true
	anim_switch("Dead", sprite)
	yield(animator, "animation_finished")
	queue_free()
	set_physics_process(false)
	
	
func patrol():
	if reset > 0 :
		reset -= 1
	if reset == 0 or is_on_wall():
		direction = dir.rand()
		direction()
		reset = randomove

func anim_switch(ani, sprite):
	if animator.current_animation != ani and ani != str(sprite.get_name()):
		sprite.visible = false
		sprite = self.get_node(ani)
		sprite.visible = true
		animator.play(ani)

func take_damage(damage):
	HEALTH -= damage
	self.modulate = Color(1,0,0)

func direction() :
	if direction == Vector2.ZERO:
		anim_switch("Idle", sprite)
	else:
		sprite.visible = false
		sprite = self.get_node("Walk")
		sprite.visible = true
		animator.play("Walk")
		if direction == Vector2.LEFT and atk.position.x > 0:
			sprite.flip_h = true
			atk.position.x = -atk.position.x - 4
			onrange.position.x = -onrange.position.x - 4
		elif direction == Vector2.RIGHT and atk.position.x < 0:
			sprite.flip_h = false
			atk.position.x = -atk.position.x - 4
			onrange.position.x = -onrange.position.x - 4
func _on_Vision_body_entered(body):
	if body == player:
		target = body
		
		
func _on_Vision_body_exited(body):
	if body == target:
		target = null
		
func attack(player):
	attacking = true
	sprite.visible = false
	sprite = self.get_node("Attack")
	sprite.visible = true
	if player.global_position < global_position:
		sprite.flip_h = true
	elif player.global_position > global_position:
		 sprite.flip_h = false
	animator.play("Attack")
	yield(animator , "animation_finished")
	
func _on_hit_body_entered(body):
	if body == player:
		player.take_damage(DAMAGE)

func _on_hit_body_exited(body):
	pass

func _on_onrange_body_entered(body):
	if body == player:
		attack(body)

func _on_onrange_body_exited(body):
	if body == player:
		attacking = false
