extends "res://Character/Character.gd"

onready var animator = self.get_node("Sprite/Anim")
onready var sprite = self.get_node("Sprite")
onready var atk = self.get_node("hit/CollisionShape2D")
onready var me = self.get_node("body")
export var DAMAGEBASH = 3
var attacking = false
var is_dead = false
var dmg = false
var pickup = false
var freeze = false
var gift = 0
var timer = 10
var count_enemy = 0
var over = false



func _physics_process(_delta):
	if HEALTH <= 0:
		if freeze:
			animator.play("freeze")
			timer = 30
			if timer > 0:
				timer -= 1
			get_tree().change_scene(str("res://Menu/GameOver.tscn"))
		else:
			dead()
	elif is_dead == false:
		punch()
		bash()
		if pickup:
			animator.play("Pickup")
			yield(animator, "animation_finished")
			pickup = false
		elif attacking == false:
			if self.modulate == Color(0,1,0) or self.modulate == Color(1,0,0):
				if timer > 0:
					timer -= 1
				else:
					self.modulate = Color(1,1,1)
			behaviour()
			movement()
			movement_controls()

func behaviour():
	if direction == Vector2.ZERO:
		animator.play("idle")
	else:
		animator.play("run")
		if(direction == Vector2.LEFT and atk.position.x > 0) or (direction == Vector2(-1,1) and atk.position.x > 0) or (direction == Vector2(-1,-1) and atk.position.x > 0)  :
			sprite.flip_h = true
			atk.position.x = -atk.position.x - 1
		elif (direction == Vector2.RIGHT and atk.position.x < 0) or (direction == Vector2(1,1) and atk.position.x < 0) or (direction == Vector2(1,-1) and atk.position.x < 0):
			sprite.flip_h = false
			atk.position.x = -atk.position.x - 1
			
			
func movement_controls():
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	
	direction.x = -int(LEFT) + int(RIGHT)
	direction.y = -int(UP) + int(DOWN)
	
func punch():
	if Input.is_action_just_pressed('punch'):
		attacking = true
		animator.play("Punch")
		yield(animator , "animation_finished" )
		attacking = false
func bash():
	if Input.is_action_just_pressed('bash'):
		attacking = true
		animator.play("Bash")
		yield(animator , "animation_finished" )
		attacking = false

func dead():
	is_dead = true
	direction = Vector2.ZERO
	animator.play("dead")
	yield(animator, "animation_finished")
	me.disabled = true
	freeze = true
	

func take_damage(damage):
	HEALTH -= damage
	self.modulate = Color(1,0,0)

func _on_Hit_body_entered(enemy):

	if enemy.TYPE == "ENEMY":
		if animator.current_animation == "Bash":
			enemy.take_damage(DAMAGEBASH)
		else:
			enemy.take_damage(DAMAGE)


func _on_Hit_body_exited(body):
	pass

