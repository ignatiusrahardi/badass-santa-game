extends KinematicBody2D

export var SPEED = 20
export var TYPE = "ENEMY"
export var DAMAGE = 1
export var HEALTH = 2


var direction = Vector2(0,0)

func movement():
	var motion
	motion = direction.normalized() * SPEED
	move_and_slide(motion, Vector2(0,0))


