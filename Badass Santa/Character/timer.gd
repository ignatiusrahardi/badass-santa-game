extends Label

onready var timer = self.get_node("timer")
var ms = 2
var s = 0
var m = 5

# Called when the node enters the scene tree for the first time.
func _process(delta):
	if ms < 1:
		s -= 1
		ms = 9
	if s < 0 :
		m -= 1
		s = 60
	if m < 0:
		timer.stop()
		m = 0
		s = 0
		ms = 0
	self.text = "Time : " + str(m)+ ":" + str(s) + ":" + str(ms)



func _on_timer_timeout():
	ms -= 1
