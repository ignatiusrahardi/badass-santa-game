extends "res://Character/Character.gd"

onready var sprite = self.get_node("AnimatedSprite")
onready var atksprite = self.get_node("atk")
onready var atkanim = self.get_node("atk/AnimationPlayer")
onready var player = self.get_parent().get_node("Player")
onready var atk = self.get_node("hit/CollisionShape2D")
onready var onrange = self.get_node("onrange/CollisionShape2D")

var randomove = 30
var reset = 0
var react_time = 300
var timer= 5
var target
var is_dead = false
var attacking = false
var count_dead = true

func _physics_process(delta):
	if HEALTH <= 0 and count_dead:
		dir.enemy_count -= 1
		count_dead = false
	elif HEALTH <= 0 and count_dead == false:
		sprite.visible = true
		atksprite.visible = false
		dead()
	elif is_dead == false:
		if attacking == false:
			movement()
			if self.modulate == Color(1,0,0):
				if timer > 0:
					timer -= 1
				else:
					self.modulate = Color(1,1,1)
			if target:
				sprite.play("Walk")
				var chase = (player.global_position - global_position).normalized() * (SPEED+10)
				if player.global_position < global_position and atk.position.x > 0:
					sprite.flip_h = true
					atk.position.x = -atk.position.x - 4
					onrange.position.x = -onrange.position.x - 4
				elif player.global_position > global_position and atk.position.x < 0:
					sprite.flip_h = false
					atk.position.x = -atk.position.x - 4
					onrange.position.x = -onrange.position.x - 4
				move_and_slide(chase,  Vector2(0,0))
			else:
				patrol()

func dead():
	is_dead = true
	direction = Vector2.ZERO
	sprite.play("Dead")
	yield(sprite, "animation_finished")
	queue_free()
	set_physics_process(false)
	
	
func patrol():
	if reset > 0 :
		reset -= 1
	if reset == 0 or is_on_wall():
		dir.menacing(sprite)
		direction()
		reset = randomove

func direction() :
	if direction == Vector2.ZERO:
		sprite.play("Idle")
	else:
		sprite.play("Walk")
		if direction == Vector2.LEFT and atk.position.x > 0:
			sprite.flip_h = true
			atk.position.x = -atk.position.x - 4
			onrange.position.x = -onrange.position.x - 4
		elif direction == Vector2.RIGHT and atk.position.x < 0:
			sprite.flip_h = false
			atk.position.x = -atk.position.x - 4
			onrange.position.x = -onrange.position.x - 4

func take_damage(damage):
	HEALTH -= damage
	self.modulate = Color(1,0,0)

func attack(player):
	attacking = true
	sprite.visible = false
	atksprite.visible = true
	if player.global_position < global_position:
		atksprite.flip_h = true
	elif player.global_position > global_position:
		 atksprite.flip_h = false
	atkanim.play("attack")
	yield(atkanim , "animation_finished" )

func _on_hit_body_entered(body):
	if body == player:
		player.take_damage(DAMAGE)

func _on_hit_body_exited(body):
	pass

func _on_onrange_body_entered(body):
	if body == player:
		attack(body)

func _on_onrange_body_exited(body):
	if body == player:
		attacking = false
		sprite.visible = true
		atksprite.visible = false

func _on_Vision_body_entered(body):
	if body == player:
		target = body
		
func _on_Vision_body_exited(body):
	if body == target:
		target = null