extends Label
onready var player = self.get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().get_node("Player")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if player.HEALTH <= 0:
		self.text = "LIFE : 0"
	else: 
		self.text = "LIFE : " + str(player.HEALTH)