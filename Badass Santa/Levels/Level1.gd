extends Node2D

onready var timer = self.get_node("HUD/Interface/MarginContainer/VBoxContainer/HBoxContainer3/Label")
onready var player = self.get_node("Player")

func _ready():
	dir.enemy_count = 0

func _process(delta):
	if dir.enemy_count == 0 and player.gift == 10:
		get_tree().change_scene(str("res://Menu/YouWin.tscn"))
	elif (timer.ms == 0 and timer.s == 0 and timer.m == 0):
		get_tree().change_scene(str("res://Menu/GameOver.tscn"))


func _on_Area2D_body_entered(body):
	if "enemy" in str(body.get_name()) :
		dir.enemy_count += 1
		
		