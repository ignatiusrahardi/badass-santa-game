extends KinematicBody2D

onready var sprite = self.get_node("Sprite")
onready var col = self.get_node("CollisionShape2D")
onready var col2 = self.get_node("pickupable/CollisionShape2D")
var timer = 15
var TYPE 
# Called when the node enters the scene tree for the first time.
func _ready():
	sprite.play("default")


func _on_pickupable_body_entered(body):
	if body.TYPE == "PLAYER":
		if body.HEALTH >= 50:
			body.HEALTH = 50
			col2.disabled = true
		else:
			col.disabled = false
			col2.disabled = false
			body.timer = 10
			body.modulate = Color(0,1,0)
			body.HEALTH += 5
			if body.HEALTH > 50:
				body.HEALTH = 50
			queue_free()
			
