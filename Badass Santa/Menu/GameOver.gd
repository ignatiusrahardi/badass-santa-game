extends Node


func _on_play_pressed():
	get_tree().change_scene("Levels/Level1.tscn")

func _on_menu_pressed():
	get_tree().change_scene("Menu/Menu.tscn")

func _on_exit_pressed():
	get_tree().quit()
