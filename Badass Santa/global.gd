extends Node

var enemy_count = 0


func rand() -> Vector2:
	var rd = randi() % 4 + 1
	match rd:
		1:
			return Vector2.RIGHT
		2:
			return Vector2.LEFT
		3:
			return Vector2.UP
		4: 
			return Vector2.DOWN
			
func menacing(body):
	var rd = randi() % 2 + 1
	match rd:
		1:
			body.flip_h = false
		2:
			body.flip_h = true
			